import {Component, OnInit, Output} from '@angular/core';
import {timer} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'data-visualization';
  loginIsShown = true;
  loadingIsShown = false;
  profileIsShown = false;

  ngOnInit(): void {
  }

  disableLogin() {
    this.loginIsShown = false;
    this.loadingIsShown = true;
  }

  disableLoading() {
    this.loadingIsShown = false;
    this.profileIsShown = true;
  }
}
