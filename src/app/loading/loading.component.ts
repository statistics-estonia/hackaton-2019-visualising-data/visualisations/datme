import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {interval, Observable, timer} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {
  @Output() eventEmitter: EventEmitter<boolean> = new EventEmitter();
  private label = 'Building your profile ';
  private dotDirection = false;
  private profileBuilt = false;
  show1 = false;
  show2 = false;
  show3 = false;
  show4 = false;
  show5 = false;
  show6 = false;

  ngOnInit(): void {
    const source = timer(500, 500);
    const subscribe = source.subscribe(val => {
      this.createLoadingDots(val);
      this.showRegisters(val);
    });
  }

  private createLoadingDots(val) {
    if (!this.profileBuilt) {
      if (val % 3 === 0) {
        this.dotDirection = !this.dotDirection;
      }
      if (this.dotDirection) {
        this.label = this.label + '.';
      } else {
        this.label = this.label.slice(0, -1);
      }
    } else {
      this.label = 'Profile ready!';
    }
  }

  endLoading() {
    this.eventEmitter.emit(false);
  }

  private showRegisters(val: number) {
    if (val === 2) {
      this.show1 = true;
    } else if (val === 3) {
      this.show2 = true;
    } else if (val === 6) {
      this.show3 = true;
    } else if (val === 10) {
      this.show4 = true;
    } else if (val === 14) {
      this.show5 = true;
    } else if (val === 15) {
      this.show6 = true;
      this.profileBuilt = true;
    }
  }
}
